package com.avengers.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;

@SpringBootApplication
public class BackendWkaApplication {

	public static void main(String[] args) {
		//SpringApplication.run(BackendWkaApplication.class, args);
		SpringApplication application = new SpringApplication(BackendWkaApplication.class);
     		application.addListeners(new ApplicationPidFileWriter());
     		application.run(args);
		
	}



}
